const { defineConfig } = require('cypress')

const createBundler = require('@bahmutov/cypress-esbuild-preprocessor')
const addCucumberPreprocessorPlugin =
	require('@badeball/cypress-cucumber-preprocessor').addCucumberPreprocessorPlugin
const createEsbuildPlugin =
	require('@badeball/cypress-cucumber-preprocessor/esbuild').createEsbuildPlugin

module.exports = defineConfig({
	projectId: 'c671o2', // <- add this line
	// projectId: 'vapeix', // <- add this line
	video: true,

	e2e: {
		///26/////
		// setupNodeEvents(on, config) {
		// 	on('task', {
		// 		hello(message) {
		// 			console.log(`hello world ! ${message}`)
		// 			return null
		// 		},
		// 		readMyFile(filename) {
		// 			const fs = require('fs')
		// 			return fs.readFileSync(`${process.cwd()}/${filename}`, 'utf-8')
		// 		},
		// 	})
		// },
		//////

		// cypress.json
		baseUrl: 'https://katalon-demo-cura.herokuapp.com/',

		async setupNodeEvents(on, config) {
			const bundler = createBundler({ plugins: [createEsbuildPlugin(config)] })
			on('file:preprocessor', bundler)
			await addCucumberPreprocessorPlugin(on, config)
			return config
		},
		// specPattern: '**/*.feature',
	},
	defaultCommandTimeout: 10000,

	watchForFileChanges: false,
	// defaultCommandTimeout: 4000,
	// execTimeout: 60000, //wait for a system command to finish executing during a cy.exec() command
	pageLoadTimeout: 180000, //wait for page load events to finish.
	// requestTimeout: 5000, //wait for an XHR request to go out
	// responseTimeout: 30000, //wait until a response

	// retries: {
	// 	// Configure retry attempts for `cypress run`
	// 	// Default is 0
	// 	runMode: 2,
	// 	// Configure retry attempts for `cypress open`
	// 	// Default is 0
	// 	openMode: 0,
	// },

	reporter: 'nyan',

	env: {
		URL: 'http://www.example.com',
		// URL: process.env('url'),
		USERNAME: 'John Doe',
		PWD: 'ThisIsNotAPassword',
	},

	// e2e: {
	setupNodeEvents(on, config) {
		// implement node event listeners here
	},
	// },

	// plugin
	// theme: 'dark',

	//BDD with cucumber
	// add it to cypress.config.js
})
