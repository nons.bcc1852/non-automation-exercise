describe('API 5: POST To Search Product', () => {
	it('API 5: POST To Search Product', () => {
		//
		const data = {
			// userId: 555,
			// id: 555,
			// title: 'I am Hacker',
			// body: 'What the heck !!!',
			search_product: 'tshirt',
		}

		cy.request({
			method: 'POST',
			url: 'https://automationexercise.com/api/searchProduct',
			body: data,
		}).as('res')

		cy.get('@res').its('body.responseCode').should('eq', 200)

		// cy.request({
		// 	method: 'PUT',
		// 	url: 'https://automationexercise.com/api/productsList',
		// 	body: data,
		// 	failOnStatusCode: false,
		// }).as('res')
	})
})
