// Add cypress/e2e/login/loginStep.js
import { Given, When, Then } from '@badeball/cypress-cucumber-preprocessor'

Given('I open login page', () => {
	cy.visit('https://katalon-demo-cura.herokuapp.com/')
	cy.get('#btn-make-appointment').click()
})

When('I fill username with {string}', username => {
	cy.get('#txt-username').type(username)
})

When('I fill password with {string}', password => {
	cy.get('#txt-password').type(password)
})

// When('I click remember me', () => {
// 	cy.get('#user_remember_me').click()
// })

When('I click login button', () => {
	cy.get('#btn-login').click()
})

Then('I can see Make Appointment Page', () => {
	cy.get('h2').should('have.text', 'Make Appointment')
})

Then('I can see alert message {string}', msg => {
	cy.get('.lead.text-danger').should('have.text', msg)
})
