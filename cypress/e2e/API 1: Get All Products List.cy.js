describe('API 1: Get All Products List', () => {
	it('API 1: Get All Products List', () => {
		//https://automationexercise.com/api/productsList
		cy.request({
			method: 'GET',
			url: 'https://automationexercise.com/api/productsList',
		}).as('res')

		cy.get('@res').its('status').should('equal', 200)

		// Response JSON: All products list
		// cy.get('@res').its('body.products').should(????????)
	})
})
