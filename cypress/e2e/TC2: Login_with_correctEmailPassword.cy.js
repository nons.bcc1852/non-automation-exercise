const exLogin = require('../page-objects/exLogin')

describe('Test Case 2: Login User with correct email and password', () => {
	const validUsername = 'JohnBoBo'
	const email = 'JohnBoBo@gmail.com'
	const validPassword = 'someValidPassword'

	it('Test Case 2: Login User with correct email and password', () => {
		exLogin.navigateToURL('http://automationexercise.com')
		// 3.Verify that home page is visible successfully
		// ?

		exLogin.clickSignUp_Login()

		exLogin.checkTextIsVisible('Login to your account')

		// // Enter correct email address and password and press Login button
		exLogin.EnterEmailAndPassword_ToLogin(email, validPassword)

		// cy.contains(` Logged in as ${validUsername}`).should('be.visible')
		exLogin.checkLoginAs(validUsername)

		exLogin.deleteAccount()
	})
})
