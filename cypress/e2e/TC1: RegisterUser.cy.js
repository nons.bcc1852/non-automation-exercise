const exLogin = require('../page-objects/exLogin')
describe('TESTCASE-5_Test Case 1: Register User', () => {
	const validUsername = 'JohnBoBo'
	const email = 'JohnBoBo@gmail.com'
	const validPassword = 'someValidPassword'
	const first_name = 'John'
	const last_name = 'Goe'
	const address1 = 'exampleAddress1Check 123 Main St'
	const address2 = 'exampleAddress2 456 Main St'

	const state = 'Middle Thailand'
	const city = 'Bangkok'
	const zipcode = '10600'
	const mobile_number = '0811111111'

	it('Test Case 1: Register User', () => {
		exLogin.navigateToURL('http://automationexercise.com')

		// 3.Verify that home page is visible successfully
		// ?

		exLogin.clickSignUp_Login()

		exLogin.checkTextIsVisible('New User Signup!')

		//Enter name and email address
		exLogin.EnterNameAndEmail_ToRegister(validUsername, email)

		exLogin.checkTextIsVisible('Enter Account Information')

		// Fill details: Title, Name, Email, Password, Date of birth
		cy.get('#id_gender1').check()
		cy.get('#password').type(validPassword)
		cy.get('select[data-qa="days"]').select('19')
		cy.get('select[data-qa="months"]').select('2')
		cy.get('select[data-qa="years"]').select('2001')

		// Select checkbox 'Sign up for our newsletter!'
		cy.get('#newsletter').check()

		// Select checkbox 'Receive special offers from our partners!'
		cy.get('#optin').check()

		// Fill details: First name, Last name, Company, Address, Address2, Country, State, City, Zipcode, Mobile Number
		cy.get('#first_name').type(first_name)
		cy.get('#last_name').type(last_name)
		cy.get('#address1').type(address1)
		cy.get('#address2').type(address2)
		cy.get('#country').select('Canada')
		cy.get('#state').type(state)
		cy.get('#city').type(city)
		cy.get('#zipcode').type(zipcode)
		cy.get('#mobile_number').type(mobile_number)

		// Create Account
		cy.get('button[data-qa="create-account"]').click()
		cy.contains('Account Created!').should('be.visible')
		cy.get('a[data-qa="continue-button"]').click()

		exLogin.checkLoginAs(validUsername)

		exLogin.deleteAccount()
	})
})
