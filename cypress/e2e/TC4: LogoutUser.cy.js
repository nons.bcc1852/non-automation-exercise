const exLogin = require('../page-objects/exLogin')

describe('Test Case 2: Login User with correct email and password', () => {
	const validUsername = 'JohnBoBo'
	const email = 'JohnBoBo@gmail.com'
	const validPassword = 'someValidPassword'

	it('Test Case 2: Login User with correct email and password', () => {
		// cy.visit('http://automationexercise.com')
		exLogin.navigateToURL('http://automationexercise.com')

		// 3.Verify that home page is visible successfully
		// ?

		// cy.contains(' Signup / Login').click()
		exLogin.clickSignUp_Login()

		// cy.contains('Login to your account').should('be.visible')
		exLogin.checkTextIsVisible('Login to your account')

		// Enter correct email address and password and click Login
		exLogin.EnterEmailAndPassword_ToLogin(email, validPassword)

		exLogin.checkLoginAs(validUsername)

		cy.contains('Logout').click()

		cy.url().should('equal', 'https://automationexercise.com/login')
	})
})
