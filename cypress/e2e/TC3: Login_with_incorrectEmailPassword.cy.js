const exLogin = require('../page-objects/exLogin')

describe('TC3: Login_with_incorrectEmailPassword.cy', () => {
	const validUsername = 'JohnBoBo'
	const email = 'JohnBoBo@gmail.com'
	const validPassword = 'someValidPassword'

	const invalidEmail = 'BohnJoJo@gmail.com'
	const invalidPassword = 'XXXsomeInValidPasswordXXX'

	it('TC3: Login_with_incorrectEmailPassword.cy', () => {
		// cy.visit('http://automationexercise.com')
		exLogin.navigateToURL('http://automationexercise.com')

		// 3.Verify that home page is visible successfully
		// ?

		exLogin.clickSignUp_Login()

		exLogin.checkTextIsVisible('Login to your account')

		// Enter correct email address and password and click Login button
		exLogin.EnterEmailAndPassword_ToLogin(invalidEmail, invalidPassword)

		exLogin.checkTextIsVisible('Your email or password is incorrect!')

		exLogin.deleteAccount()
	})
})
