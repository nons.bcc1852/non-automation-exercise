const exLogin = require('../page-objects/exLogin')

describe('TESTCASE-5_Test Case 1: Register User', () => {
	const validUsername = 'JohnBoBo'
	const email = 'JohnBoBo@gmail.com'

	it('Test Case 1: Register User', () => {
		// cy.visit('http://automationexercise.com')
		exLogin.navigateToURL('http://automationexercise.com')

		// 3.Verify that home page is visible successfully
		// ?

		// cy.contains(' Signup / Login').click()
		exLogin.clickSignUp_Login()

		// cy.contains('New User Signup!').should('be.visible')
		exLogin.checkTextIsVisible('New User Signup!')

		// Enter name and email address and click Register button
		exLogin.EnterNameAndEmail_ToRegister(validUsername, email)

		// cy.contains('Email Address already exist!').should('be.visible')
		exLogin.checkTextIsVisible('Email Address already exist!')
	})
})
