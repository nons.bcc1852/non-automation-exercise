describe('API 7: POST To Verify Login with valid details', () => {
	it('API 7: POST To Verify Login with valid details', () => {
		//
		const email = 'JohnBoBo@gmail.com'
		const validPassword = 'someValidPassword'

		cy.request({
			method: 'POST',
			url: 'https://automationexercise.com/api/verifyLogin',
			body: {
				email: email,
				password: validPassword,
			},
		}).as('res')

		cy.get('@res').its('status').should('equal', 200)
		cy.get('@res').its('status').should('equal', 200)
	})
})
