const Page = require('./Page')

class exLogin extends Page {
	static txtSignupName = 'input[data-qa="signup-name"]'
	static txtSignupEmail = 'input[data-qa="signup-email"]'
	static btnSignUp = 'button[data-qa="signup-button"'

	static clickSignUp_Login() {
		cy.contains(' Signup / Login').click()
	}

	static checkTextIsVisible(textToCheck) {
		cy.contains(textToCheck).should('be.visible')
	}

	static EnterNameAndEmail_ToRegister(validUsername, email) {
		// Enter name
		cy.get(this.txtSignupName).type(validUsername)
		// Enter email address
		cy.get(this.txtSignupEmail).type(email)
		// Click Signup button
		cy.get(this.btnSignUp).click()
	}

	static checkLoginAs(validUsername) {
		cy.contains(` Logged in as ${validUsername}`).should('be.visible')
	}

	static deleteAccount() {
		cy.contains(' Delete Account').click()
		cy.contains('Account Deleted!').should('be.visible')
		cy.contains('Continue').click()
	}

	///////

	static txtLoginEmail = 'input[data-qa="login-email"]'
	static txtLoginPassword = 'input[data-qa="login-password"]'

	static EnterEmailAndPassword_ToLogin(email, validPassword) {
		// Enter correct email address and password
		// Enter correct email address
		cy.get('input[data-qa="login-email"]').type(email)

		// Enter correct password
		cy.get('input[data-qa="login-password"]').type(validPassword)

		// Click Login button
		cy.get('button[data-qa="login-button"]').click()
	}

	//////

	static btnMakeAppoint = '#btn-make-appointment'
	static txtUsername = '#txt-username'
	static txtPassword = '#txt-password'
	static btnLogin = '#btn-login'
	static divAlertMessage = '.lead.text-danger'

	static clickMakeAppoint() {
		cy.get(this.btnMakeAppoint).click()
	}

	static inputLoginForm(username, password) {
		cy.get(this.txtUsername).type(username)
		cy.get(this.txtPassword).type(password)
		cy.get(this.btnLogin).click()
	}

	static canDisplayAlert(expectedAlertMessage) {
		cy.get(this.divAlertMessage).should('have.text', expectedAlertMessage)
	}
}

module.exports = exLogin
